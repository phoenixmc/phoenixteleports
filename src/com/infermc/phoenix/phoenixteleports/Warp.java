package com.infermc.phoenix.phoenixteleports;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;

public class Warp {
    private Location location;
    private Location destination = null;
    private String name;
    private Effect effect = Effect.VOID_FOG;
    private String permission;

    private boolean visible = true;
    private boolean enabled = true;
    private boolean needperm = false;

    private double speed = .05;
    private double radius = .5;
    private double height = 2;

    public double cDeg = 0;

    private int requiredLevel = 0;
    private boolean requiresLegend = false;

    public Warp(String n, Location loc) {
        name = n;
        permission = "phoenixteleports.use."+n.toLowerCase();
        location = loc;
        centerLocation();
    }

    // Getters
    public Location getLocation() {
        return location;
    }
    public Location getDestination() {
        return destination;
    }
    public String getName() {
        return name;
    }
    public Effect getEffect() {
        return effect;
    }
    public String getPermission() {
        return permission;
    }
    public boolean isVisible() {
        return visible;
    }
    public boolean isEnabled() {
        return enabled;
    }
    public boolean requiresPermission() {
        return needperm;
    }
    public double getSpeed() {
        return speed;
    }
    public double getRadius() {
        return radius;
    }
    public double getHeight() {
        return height;
    }
    public int getRequiredLevel() { return requiredLevel; }

    // Setters
    public void setLocation(Location loc) {
        location = loc;
        centerLocation();
    }
    public void setDestination(Location dest) {
        destination = dest;
    }
    public void setName(String n) {
        name = n;
    }
    public void setEffect(Effect eff) {
        effect = eff;
    }
    public void setEnabled(boolean en) {
        enabled = en;
    }
    public void setVisible(boolean vis) {
        visible = vis;
    }
    public void setPermission(String p) {
        permission = p.toLowerCase();
    }
    public void setRequiresPermission(boolean req) {
        needperm = req;
    }
    public void setSpeed(double s) {
        speed = s;
    }
    public void setRadius(double r) {
        radius = r;
    }
    public void setHeight(double h) {
        height = h;
    }
    public void setRequiredLevel(int level) { requiredLevel = level; }
    public void setRequiresLegend(boolean r) { requiresLegend = r; }

    // Other
    public void centerLocation() {
        // Gets the exact center of the particle location.
        location = new Location(location.getWorld(),location.getBlockX()+.55,location.getBlockY(),location.getBlockZ()+.55);
    }
    public boolean requiresLegend() {
        return this.requiresLegend;
    }

    public void tick() {

        // Stuff that is not relevant to the shape
        // Center Beam
        //location.getWorld().playEffect(new Location(location.getWorld(),location.getX(),location.getY(),location.getZ()),effect,0);

        double x=0;
        double z=0;

        x = location.getX() + radius * Math.cos(this.cDeg);
        z = location.getZ() + radius * Math.sin(this.cDeg);

        if (this.cDeg <= 360) {
            this.cDeg += speed * 2;
        } else {
            this.cDeg = 0;
        }

        // Radial Beams
        for (double y = 0; y <= height; y += speed * 2) {
            location.getWorld().playEffect(new Location(location.getWorld(), x, location.getY() + y, z), effect, 1);
            //loc.getWorld().playEffect(new Location(loc.getWorld(), x2, loc.getY()+y, z2), effect, 1);
        }
    }
}
