package com.infermc.phoenix.phoenixteleports;

import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.profiles.Profile;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class PhoenixTeleports extends JavaPlugin implements Listener {
    ArrayList<Warp> warps = new ArrayList<Warp>();
    HashMap<Player,Warp> Colliding = new HashMap<Player, Warp>();
    File warpsFile;
    ConfigurationSection defaults = null;
    PhoenixProfiles profiles;

    public void onEnable() {
        warpsFile = new File(getDataFolder()+"/warps.yml");
        double configVersion = 0.1;
        saveDefaultConfig();
        if (getConfig().getDouble("version") != configVersion) {
            getLogger().info("WARNING: Configuration is outdated! Please consider updating it!");
        }
        defaults = getConfig().getConfigurationSection("defaults");

        getServer().getPluginManager().registerEvents(this,this);
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

        getLogger().info("Loading warps");
        loadWarps();

        if (getServer().getPluginManager().isPluginEnabled("PhoenixProfiles")) {
            profiles = (PhoenixProfiles) getServer().getPluginManager().getPlugin("PhoenixProfiles");
        } else {
            getLogger().info("Missing PhoenixProfiles!");
        }

        scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for (Warp w : warps) {
                    if (w.isVisible()) w.tick();
                }
            }
        }, 0L, 1L);

        saveResource("usage.txt",true);
    }
    public void onDisable() {
        saveWarps();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = null;
        if (sender instanceof Player) player = (Player) sender;

        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("create") && sender.hasPermission("phoenixteleports.create")) {
                if (player != null) {
                    Warp w = getWarp(args[1]);
                    if (w == null) {
                        Warp existing = findWarp(player.getLocation());
                        if (existing == null) {
                            createWarp(args[1], player.getLocation());
                            player.sendMessage("Warp created!");
                        } else {
                            player.sendMessage("Cannot create warp here! Warp "+existing.getName()+" is already here!");
                        }
                    } else {
                        player.sendMessage("Warp with that name already exists!");
                    }
                } else {
                    sender.sendMessage("User only command :(");
                }
                return true;
            } else if ((args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("destroy")) && sender.hasPermission("phoenixteleports.delete")) {
                if (args.length > 1) {
                    deleteWarp(args[1]);
                } else {
                    sender.sendMessage("Syntax: /int delete|destroy name");
                }
                return true;
            } else if (args[0].equalsIgnoreCase("set") && sender.hasPermission("phoenixteleports.set")) {
                if (args.length < 3) {
                    sender.sendMessage("Syntax: /int set name destination|location|particle|radius|speed|level values");
                } else {
                    Warp warp = getWarp(args[1]);
                    if (warp != null) {
                        if (args[2].equalsIgnoreCase("location")) {
                            // Update warps location
                            if (player != null) {
                                warp.setLocation(player.getLocation());
                                player.sendMessage("Warp location updated.");
                                saveWarps();
                            } else {
                                sender.sendMessage("This is a user only command!");
                            }
                        } else if (args[2].equalsIgnoreCase("destination")) {
                            // Update warps destination
                            if (player != null) {
                                warp.setDestination(player.getLocation());
                                player.sendMessage("Warp destination set!");
                                saveWarps();
                            } else {
                                sender.sendMessage("This is a user only command!");
                            }
                        } else if (args[2].equalsIgnoreCase("effect") && args.length >= 4) {
                            // Changes the effect
                            Effect ef = getEffect(args[3]);
                            if (ef == null) {
                                player.sendMessage("No such effect :(");
                            } else {
                                if (ef.getType() == Effect.Type.SOUND) {
                                    player.sendMessage("Sounds effects cannot be used!");
                                } else {
                                    warp.setEffect(ef);
                                    player.sendMessage("Warp effect updated.");
                                    saveWarps();
                                }
                            }
                        } else if (args[2].equalsIgnoreCase("radius") && args.length >= 4) {
                            try {
                                Double rad = Double.valueOf(args[3]);
                                warp.setRadius(rad);
                                player.sendMessage("Warp radius updated!");
                                saveWarps();
                            } catch (NumberFormatException e) {
                                player.sendMessage("Radius must be a number!");
                            }
                        } else if (args[2].equalsIgnoreCase("speed") && args.length >= 4) {
                            try {
                                Double speed = Double.valueOf(args[3]);
                                if (speed < 0) {
                                    player.sendMessage("Speed cannot be negative!");
                                } else {
                                    warp.setSpeed(speed);
                                    player.sendMessage("Warp speed updated!");
                                    saveWarps();
                                }
                            } catch (NumberFormatException e) {
                                player.sendMessage("Speed must be a number!");
                            }
                        } else if (args[2].equalsIgnoreCase("height") && args.length >= 4) {
                            try {
                                Double height = Double.valueOf(args[3]);
                                if (height < 0) {
                                    player.sendMessage("Height cannot be negative!");
                                } else {
                                    warp.setHeight(height);
                                    player.sendMessage("Warp height updated!");
                                    saveWarps();
                                }
                            } catch (NumberFormatException e) {
                                player.sendMessage("Height must be a number!");
                            }
                        } else if (args[2].equalsIgnoreCase("level") && args.length >= 4) {
                            try {
                                Integer level = Integer.valueOf(args[3]);
                                warp.setRequiredLevel(level);
                                if (args.length >= 5) {
                                    if (args[4].equalsIgnoreCase("--legend")) {
                                        warp.setRequiresLegend(true);
                                    } else {
                                        warp.setRequiresLegend(false);
                                    }
                                } else {
                                    warp.setRequiresLegend(false);
                                }
                                player.sendMessage("Warp level updated!");
                                saveWarps();
                            } catch (NumberFormatException e) {
                                player.sendMessage("Level must be a number!");
                            }
                        } else {
                            player.sendMessage("Unknown warp property '"+args[2].toLowerCase()+"'");
                        }
                    } else {
                        player.sendMessage("No such warp!");
                    }
                }
                return true;
            } else if (args[0].equalsIgnoreCase("toggle") && sender.hasPermission("phoenixteleports.toggle")) {
                Warp warp = getWarp(args[1]);
                if (warp != null) {
                    if (args[2].equalsIgnoreCase("enabled")) {
                        warp.setEnabled(!warp.isEnabled());
                        if (warp.isEnabled()) {
                            sender.sendMessage("Warp is now enabled!");
                        } else {
                            sender.sendMessage("Warp is now disabled.");
                        }
                        saveWarps();
                    } else if (args[2].equalsIgnoreCase("permission")) {
                        warp.setRequiresPermission(!warp.requiresPermission());
                        if (warp.requiresPermission()) {
                            sender.sendMessage("Warp now requires permission ("+warp.getPermission()+")!");
                        } else {
                            sender.sendMessage("Warp is now public and doesn't require permission!");
                        }
                        saveWarps();
                    } else if (args[2].equalsIgnoreCase("visible")) {
                        warp.setVisible(!warp.isVisible());
                        if (warp.isVisible()) {
                            sender.sendMessage("Warp is now visible.");
                        } else {
                            if (warp.isEnabled()) {
                                sender.sendMessage("Warp is now hidden but is enabled and will still function.");
                            } else {
                                sender.sendMessage("Warp is now hidden.");
                            }
                        }
                        saveWarps();
                    } else {
                        sender.sendMessage("Syntax: /int toggle name enabled|permission|visible");
                    }
                } else {
                    sender.sendMessage("No such warp!");
                }
                return true;
            } else if (args[0].equalsIgnoreCase("center") && sender.hasPermission("phoenixteleports.center")) {
                Warp w = getWarp(args[1]);
                if (w != null) {
                    w.centerLocation();
                    sender.sendMessage("Warp location has been recentered.");
                } else {
                    sender.sendMessage("No such warp!");
                }
                saveWarps();
            } else if (args[0].equalsIgnoreCase("list") && sender.hasPermission("phoenixteleports.list")) {
                sender.sendMessage("Phoenix Teleports Warp List:");
                String warplist = "";
                for (Warp w : warps) {
                    warplist += w.getName()+", ";
                }
                sender.sendMessage(warplist);
                sender.sendMessage("Listed a total of "+warps.size()+" warps.");
                return true;
            } else if (args[0].equalsIgnoreCase("info") && sender.hasPermission("phoenixteleports.info")) {
                if (args.length < 2) {
                    sender.sendMessage("Syntax: /int info name");
                } else {
                    Warp warp = getWarp(args[1]);
                    if (warp != null) {
                        sender.sendMessage("Warp name: "+warp.getName());

                        Location l = warp.getLocation();
                        sender.sendMessage("Location: "+l.getWorld().getName()+", "+l.getX()+", "+l.getY()+", "+l.getZ());

                        Location d = warp.getDestination();
                        if (d != null) {
                            sender.sendMessage("Destination: " + d.getWorld().getName() + ", " + d.getX() + ", " + d.getY() + ", " + d.getZ() + ", " + d.getYaw() + ", " + d.getPitch());
                        } else {
                            sender.sendMessage("Destination: none");
                        }

                        sender.sendMessage("Visible: "+(warp.isVisible()==true ? "Yes" : "No"));
                        sender.sendMessage("Enabled: "+(warp.isEnabled()==true ? "Yes" : "No"));
                        sender.sendMessage("Requires permission to use: "+(warp.requiresPermission()==true ? "Yes" : "No"));
                        sender.sendMessage("Warp permission: "+warp.getPermission());

                        sender.sendMessage("Effect: "+warp.getEffect().toString());

                        sender.sendMessage("Speed: "+warp.getSpeed());
                        sender.sendMessage("Radius: "+warp.getRadius());
                        sender.sendMessage("Height: "+warp.getHeight()+" blocks");
                    } else {
                        sender.sendMessage("No such warp!");
                    }
                }
                return true;
            } else if (args[0].equalsIgnoreCase("save") && sender.hasPermission("phoenixteleports.save")) {
                saveWarps();
                sender.sendMessage("Saved warps to disk.");
                return true;
            } else if (args[0].equalsIgnoreCase("load") && sender.hasPermission("phoenixteleports.load")) {
                loadWarps();
                sender.sendMessage("Loaded warps from disk.");
                return true;
            } else if (args[0].equalsIgnoreCase("goto") && sender.hasPermission("phoenixteleports.goto")) {
                if (player != null) {
                    Warp warp = getWarp(args[1]);
                    if (warp != null) {
                        Colliding.put(player,warp);
                        player.teleport(warp.getLocation());
                    } else {
                        sender.sendMessage("No such warp!");
                    }
                } else {
                    sender.sendMessage("User only command!");
                }
                return true;
            }
        } else {
            sender.sendMessage("/int [create|delete|destroy|set|toggle|list|info|center|save|load|goto]");
        }
        return true;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent ev) {
        // If they disconnected in a portal, don't teleport them!
        Warp warp = findWarp(ev.getPlayer().getLocation());
        if (warp != null) Colliding.put(ev.getPlayer(),warp);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent ev) {
        Location loc = ev.getTo();
        Location location = new Location(loc.getWorld(),loc.getBlockX()+.55,loc.getBlockY(),loc.getBlockZ()+.55);
        if (Colliding.containsKey(ev.getPlayer())) {
            if (!Colliding.get(ev.getPlayer()).getLocation().equals(location)) {
                //getLogger().info("Exited warp.");
                Colliding.remove(ev.getPlayer());
            }
        }

        for (Warp w : warps) {
            if (w.getLocation().equals(location)) {
                if (!Colliding.containsKey(ev.getPlayer())) {
                    //getLogger().info("Collided, teleporting");
                    Colliding.put(ev.getPlayer(),w);

                    if (w.isEnabled()) {
                        boolean hasPerms = false;
                        if (w.requiresPermission()) {
                            if (ev.getPlayer().hasPermission(w.getPermission())) hasPerms = true;
                        } else {
                            hasPerms = true;
                        }

                        if (hasPerms) {
                            if (w.getDestination() == null) {
                                ev.getPlayer().sendMessage("No destination!");
                            } else {
                                Profile profile = null;
                                if (profiles != null) profile = profiles.getProfileManager().getProfile(ev.getPlayer());

                                int normalLevel = 0;
                                int legendLevel = 0;

                                if (profile != null) {
                                    normalLevel = profile.getLevel();
                                    legendLevel = profile.getLegendLevel();
                                }

                                if ( (!w.requiresLegend() && w.getRequiredLevel() <= normalLevel) || (w.requiresLegend() && w.getRequiredLevel() <= legendLevel)) {
                                    // In the event portals overlap and the player teleports into one
                                    // We'll set them as already colliding in the destination portal
                                    // So they don't get stuck.
                                    Warp existing = findWarp(w.getDestination());
                                    if (existing == null) {
                                        Colliding.remove(ev.getPlayer());
                                    } else {
                                        Colliding.put(ev.getPlayer(), existing);
                                    }
                                    ev.getPlayer().teleport(w.getDestination());

                                } else {
                                    if (w.requiresLegend()) {
                                        ev.getPlayer().sendMessage(ChatColor.RED+"You must be at least Legendary Level "+w.getRequiredLevel()+" to use this warp!");
                                    } else {
                                        ev.getPlayer().sendMessage(ChatColor.RED+"You must be at least Level "+w.getRequiredLevel()+" to use this warp!");
                                    }
                                }
                            }
                        } else if (w.requiresPermission()) {
                            ev.getPlayer().sendMessage("You do not have permission to use this warp!");
                        }
                    } else {
                        if (w.isVisible()) ev.getPlayer().sendMessage("Warp is disabled!");
                    }
                    return;
                }
            }
        }
    }

    // Creates a warp at the specified location with configured defaults
    public boolean createWarp(String name, Location loc) {
        Warp warp = new Warp(name,loc);

        if (defaults != null) {
            Effect eff = getEffect(defaults.getString("effect"));
            if (eff != null) warp.setEffect(eff);

            boolean visible = defaults.getBoolean("visible",true);
            boolean enabled = defaults.getBoolean("enabled",true);
            boolean needperm = defaults.getBoolean("needperm",false);
            double speed = defaults.getDouble("speed",0.05);
            double radius = defaults.getDouble("radius",0.5);
            double height = defaults.getDouble("height",2);

            warp.setVisible(visible);
            warp.setEnabled(enabled);
            warp.setRequiresPermission(needperm);
            warp.setSpeed(speed);
            warp.setRadius(radius);
            warp.setHeight(height);
        }

        warps.add(warp);
        saveWarps();
        return false;
    }
    public Warp getWarp(String name) {
        for (Warp w : warps) {
            if (w.getName().equalsIgnoreCase(name)) return w;
        }
        return null;
    }
    public void deleteWarp(String name) {
        Warp w = getWarp(name);
        if (w != null) {
            warps.remove(w);
            saveWarps();
        }
    }

    public void saveWarps() {
        YamlConfiguration cfg = new YamlConfiguration();
        for (Warp w : warps) {
            HashMap<String, Object> warpSection = new HashMap<String, Object>();

            HashMap<String,Object> location = new HashMap<String, Object>();
            Location l = w.getLocation();
            location.put("world",l.getWorld().getName());
            location.put("x",l.getX());
            location.put("y",l.getY());
            location.put("z",l.getZ());
            warpSection.put("location",location);

            HashMap<String,Object> destination = new HashMap<String, Object>();
            Location d = w.getDestination();
            if (d != null) {
                warpSection.put("destination", d);
            }

            warpSection.put("effect",w.getEffect().toString());
            warpSection.put("permission",w.getPermission());
            warpSection.put("needspermission",w.requiresPermission());
            warpSection.put("visible",w.isVisible());
            warpSection.put("enabled",w.isEnabled());
            warpSection.put("speed",w.getSpeed());
            warpSection.put("radius",w.getRadius());
            warpSection.put("height",w.getHeight());

            warpSection.put("requiredLevel",w.getRequiredLevel());
            warpSection.put("requiresLegend",w.requiresLegend());

            cfg.createSection(w.getName(),warpSection);
        }
        // Ensure we can save it in the right place.
        if (!getDataFolder().exists()) getDataFolder().mkdirs();
        try {
            cfg.save(warpsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void loadWarps() {
        warps.clear();
        if (warpsFile.exists()) {
            YamlConfiguration cfg = YamlConfiguration.loadConfiguration(warpsFile);
            if (cfg != null) {
                for (String warpName : cfg.getKeys(false)) {
                    // Begin loading the warps :D (Killme)
                    ConfigurationSection warpSection = cfg.getConfigurationSection(warpName);

                    // Load the location
                    ConfigurationSection locationSection = warpSection.getConfigurationSection("location");
                    if (locationSection == null) {
                        getLogger().info("FATAL: "+warpName+" doesn't have a location, Skipping "+warpName+"!");
                        continue;
                    }
                    World locationWorld = getServer().getWorld(locationSection.getString("world"));
                    if (locationWorld == null) {
                        getLogger().info("FATAL: World "+locationSection.getString("world")+" doesn't exist, Skipping "+warpName+"!");
                        continue;
                    }
                    double locationX = locationSection.getDouble("x");
                    double locationY = locationSection.getDouble("y");
                    double locationZ = locationSection.getDouble("z");
                    Location location = new Location(locationWorld,locationX,locationY,locationZ);

                    Warp warp = new Warp(warpName,location);

                    if (warpSection.contains("destination")) {
                        warp.setDestination((Location) warpSection.get("destination"));
                    }

                    Effect effect = getEffect(warpSection.getString("effect"));
                    if (effect != null) warp.setEffect(effect);

                    String permission = warpSection.getString("permission",null);
                    if (permission != null) warp.setPermission(permission);

                    boolean requiresPerm = warpSection.getBoolean("needspermission",false);
                    warp.setRequiresPermission(requiresPerm);

                    boolean visible = warpSection.getBoolean("visible",true);
                    warp.setVisible(visible);

                    boolean enabled = warpSection.getBoolean("enabled",true);
                    warp.setEnabled(enabled);

                    double speed = warpSection.getDouble("speed");
                    warp.setSpeed(speed);

                    double radius = warpSection.getDouble("radius");
                    warp.setRadius(radius);

                    double height = warpSection.getDouble("height");
                    warp.setHeight(height);

                    Integer requiredLevel = warpSection.getInt("requiredLevel",0);
                    warp.setRequiredLevel(requiredLevel);

                    boolean requiresLegend = warpSection.getBoolean("requiresLegend",false);
                    warp.setRequiresLegend(requiresLegend);

                    warps.add(warp);
                }

                if (warps.size() > 0) {
                    if (warps.size() == 1) {
                        getLogger().info("Loaded " + warps.size() + " warp!");
                    } else {
                        getLogger().info("Loaded " + warps.size() + " warps!");
                    }
                } else {
                    getLogger().info("No warps to load");
                }
            } else {
                getLogger().info("Error loading warps :(");
            }
        } else {
            getLogger().info("No warps file, No warps to load");
        }
    }

    // Custom wrapper to match the JavaDoc page Enums
    public Effect getEffect(String name) {
        for (Effect eff : Effect.values()) {
            if (eff.toString().equalsIgnoreCase(name)) return eff;
        }
        return null;
    }

    public boolean locationCollision(Location a, Location b) {
        Location aa = new Location(a.getWorld(),a.getBlockX()+.55,a.getBlockY(),a.getBlockZ()+.55);
        Location bb = new Location(b.getWorld(),b.getBlockX()+.55,b.getBlockY(),b.getBlockZ()+.55);
        if (aa.equals(bb)) return true;
        return false;
    }
    public Warp findWarp(Location loc) {
        for (Warp w : warps) {
            if (locationCollision(w.getLocation(),loc)) return w;
        }
        return null;
    }
}
